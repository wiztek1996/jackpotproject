﻿using System;

[Serializable]
public class BetConfig
{
    public BetType type;
    public int bonus;
}

public enum BetType
{
    BAR = 0,
    SEVEN = 1,
    STAR = 2,
    WATER_MELON = 3,
    BELL = 4,
    CHERRY = 5,
    ORANGE = 6,
    APPLE = 7,
    LUCKY = 8
}
