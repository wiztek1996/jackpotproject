﻿public class BetData
{
    public BetType type;
    public int score;

    public BetData(BetType type, int score)
    {
        this.type = type;
        this.score = score;
    }
}