﻿using System;

[Serializable]
public class BlockData
{
    [NonSerialized] public int id;
    public BetType type;
    public int credit;
    public string GetTypeName()
    {
        string typeName = "NONE";
        switch (type)
        {
            case BetType.BAR : typeName = "BAR" ; break;
            case BetType.SEVEN : typeName = "SEVEN" ; break;
            case BetType.STAR : typeName = "STAR" ; break;
            case BetType.WATER_MELON : typeName = "WATER MELON" ; break;
            case BetType.BELL : typeName = "BELL" ; break;
            case BetType.CHERRY : typeName = "CHERRY" ; break;
            case BetType.ORANGE : typeName = "ORANGE" ; break;
            case BetType.APPLE : typeName = "APPLE" ; break;
            case BetType.LUCKY : typeName = "LUCKY" ; break;
        }
        return typeName;
    }
    
    public BlockData(int id,BetType type ,int credit)
    {
        this.id = id;
        this.type = type;
        this.credit = credit;
    }
    
    
}