﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class BetController : MonoBehaviour
{
    [SerializeField] private Transform _tranGroupBtn;
    private Button[] _arrBtnOtion = new Button[8];
    private Dictionary<BetType, BetData> _dicBet = new Dictionary<BetType, BetData>();

    public Dictionary<BetType, BetData> DicBet
    {
        get => _dicBet;
        set => _dicBet = value;
    }


    private void Awake()
    {
        InitBtnOption();
        Service.Set(this);
    }

    private void InitBtnOption()
    {
        for (int i = 0; i < _tranGroupBtn.childCount; i++)
        {
            _arrBtnOtion[i] = _tranGroupBtn.GetChild(i).GetComponent<Button>();
            _tranGroupBtn.GetChild(i).GetComponent<Image>().sprite = LoadFromResources.GetSpriteBetType((BetType)i);
            var index = i;
            _arrBtnOtion[i].onClick.AddListener(() => ClickToBet(index));
        }
    }

    private void ClickToBet(int index)
    {
        //sub current credit &Add score to bet Option
        if (Service.PLAYER.SubCredit() >= 0)
        {
            Service.SOUND.PlaySoundInsertCoin(index);
            BetType type = (BetType) index;
            TextMeshProUGUI txtScore = _arrBtnOtion[index].transform.GetChild(0).GetComponent<TextMeshProUGUI>();
            if (!_dicBet.ContainsKey(type)) _dicBet.Add(type, new BetData(type, 0));
            _dicBet[type].score += Service.PLAYER.DefaultBetValue;
            txtScore.text = _dicBet[type].score.ToString();
            //Show score to screen
            Debug.LogWarning(type + " SELECTED ["+" SCORE "+_dicBet[type].score+"]");
        }
        else Service.CREDIT.ShowNotEnoughCredit();
        Service.SPIN.SetBtnSpinStatus();
    }

    public void StatusBtnOptionBet(bool val)
    {
        if (val)
        {
            long creditRefund = 0;
            foreach (var betData in _dicBet.Values)
            {
                creditRefund += betData.score;
                betData.score = 0;
            }

            if (creditRefund > 0)
            {
                Service.SOUND.PlaySoundFx(SoundName.Transfer_Coin);
                Debug.LogWarning("Refund To Player Credit " + creditRefund);
                Service.PLAYER.AddPlayerCredit(creditRefund);
            }
            else Service.SOUND.PlaySoundFx(SoundName.Touch_Lcd_Button_Not_Ok);
        }
        ResetAllBetScore();
    }

    public void EnableAllBetOption(bool val)
    {
        foreach (var btnBet in _arrBtnOtion)
        {
            btnBet.interactable = val;
        }
    }
    private void ResetAllBetScore()
    {
        foreach (var btnOption in _arrBtnOtion)
        {
            btnOption.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = 0.ToString();
        }
    }

    public bool CheckConditionBet()
    {
        foreach (var betData in _dicBet.Values)
        {
            if (betData.score > 0) return true;
        }

        return false;
    }

    public void FocusWinEffect(BetType type)
    {
        Helper.ClickEffect(_arrBtnOtion[(int) type].transform,1.6f);
    }
}