﻿using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public partial class PlayerConfig : MonoBehaviour
{
    public GameConfig gameConfig;
     private int _defaultBetValue;
     private long _playerCredit;
    private static readonly Dictionary<int,BlockData> _dicBlockdata = new Dictionary<int, BlockData>();
    [SerializeField] private BlockData[] _arrBlockConfig = new BlockData[24];
    [SerializeField] private BetConfig[] _arrBetConfig = new BetConfig[8];

    public  Dictionary<int, BlockData>  DicBlockdata => _dicBlockdata;

    public long PlayerCredit => _playerCredit;
    public int DefaultBetValue => _defaultBetValue;
    public long WinCredit { get; private set; }
    
    public Transform _transBetDetails;

    public void InitConfig()
    {
        _playerCredit = gameConfig.playerCredit;
        _defaultBetValue = gameConfig.defaultBetValue;
    }

    private void InitBlock()
    {
        for (int i = 0; i < _arrBlockConfig.Length; i++)
        {
            _dicBlockdata.Add(i,new BlockData(i,_arrBlockConfig[i].type,_arrBlockConfig[i].credit));
        }
    }

    private void InitBetOption()
    {
        for (int i = 0; i < _arrBetConfig.Length; i++)
        {
            _transBetDetails.GetChild(i).GetComponentInChildren<TextMeshProUGUI>().text = "x" + _arrBetConfig[i].bonus;
        }
    }
    
    private void Awake()
    {
        Service.Set(this);
        InitConfig();
        InitBlock();
        InitBetOption();
    }

    public void MoveCreditToPlayer()
    {
        Service.SOUND.PlaySoundFx(SoundName.Transfer_Coin);
        AddPlayerCredit(WinCredit);
        WinCredit = 0;
        Service.CREDIT.UpdateWinCredit();
    }

    public BetConfig GetBetConfig(BetType type)
    {
        foreach (var betConfig in _arrBetConfig)
        {
            if (betConfig.type == type) return betConfig;
        }

        return null;
    }

    public BlockData GetBlockData(int index)
    {
        return _dicBlockdata[index];
    }
}
