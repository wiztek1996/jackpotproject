﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class DialogController : MonoBehaviour
{
    [SerializeField] private Button _btnExit, _btnSettings;
    [NonSerialized]  public GameObject BG;
    [SerializeField] private DialogPromptExit _dialogPromptExit;
    [SerializeField] private DialogSettings _dialogSettings;

    private void Awake()
    {
        BG = transform.GetChild(0).gameObject;
        _btnExit.onClick.AddListener(ShowPromptExitDialog);
        _btnSettings.onClick.AddListener(ShowSettingsDialog);
        ClickToCancel();
        Service.Set(this);
    }

    private void ShowPromptExitDialog()
    {
        Service.SOUND.PlaySoundFx(SoundName.Touch_Lcd_Button_Ok);
        Helper.ClickEffect(_btnExit.transform);
        _dialogPromptExit.Show();
    }
    
    private void ShowSettingsDialog()
    {
        Service.SOUND.PlaySoundFx(SoundName.Touch_Lcd_Button_Ok);
        Helper.ClickEffect(_btnSettings.transform);
        _dialogSettings.Show();
    }

    public void ClickToCancel()
    {
        Helper.ActiveAllChild(transform, false);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            ShowPromptExitDialog();
        }
    }
}