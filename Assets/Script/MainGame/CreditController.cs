﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class CreditController : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI _txtCredit, _txtCreditWin;
    private TextNumberHelper _txtAnimPlayer, _txtAnimWin;
    private void Awake()
    {
        InitCredit();
    }

    private void InitCredit()
    {
        Service.SPIN.SetBtnSpinStatus(false);
        _txtCredit.text = Helper.NumberGroup(Service.PLAYER.PlayerCredit);
        _txtCreditWin.text = Helper.NumberGroup(Service.PLAYER.WinCredit);
        _txtAnimPlayer = _txtCredit.GetComponent<TextNumberHelper>();
        _txtAnimWin = _txtCreditWin.GetComponent<TextNumberHelper>();
        Service.Set(this);
    }

    public void UpdatePlayerCredit()
    {
        _txtAnimPlayer.IncreaseTo(Service.PLAYER.PlayerCredit);
    }

    public void UpdateWinCredit()
    {
        _txtAnimWin.IncreaseTo(Service.PLAYER.WinCredit);
    }

    public void ShowNotEnoughCredit()
    {
        Service.SOUND.PlaySoundFx(SoundName.Touch_Lcd_Button_Not_Ok);
        Debug.LogError("NOT ENOUGH CREDIT ,PLS RECHARGE FIRST !");
    }
}