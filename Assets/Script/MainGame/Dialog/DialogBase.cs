﻿using System;
using DG.Tweening;
using UnityEngine;

public class DialogBase : MonoBehaviour
{
    private const float START_SCALE_DIALOG = 0.6f;
    private const float TIME_OPEN_DIALOG = 0.18f;
    
    private void OnEnable()
    {
        OpenDialog();
    }

    private void OnDisable()
    {
        Service.DIALOG.BG.SetActive(false);
        ResetDialog();
    }

    private void ResetDialog()
    {
        transform.localScale = new Vector3(START_SCALE_DIALOG, START_SCALE_DIALOG);
    }
    

    protected void OpenDialog()
    {
        ResetDialog();
        Service.DIALOG.BG.SetActive(true);
        transform.DOScale(new Vector3(1, 1), TIME_OPEN_DIALOG).SetEase(Ease.OutFlash);
    }

    public void Show(bool val= true)
    {
        gameObject.SetActive(val);
    }
    
}