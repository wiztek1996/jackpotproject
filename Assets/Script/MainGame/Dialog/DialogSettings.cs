﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class DialogSettings : DialogBase
{
    [SerializeField] private Toggle _tgSoundBG, _tgSoundFX;
    [SerializeField] private Button _btnExit;
    
    private void Awake()
    {
        _btnExit.onClick.AddListener(() =>
        {
            Service.SOUND.PlaySoundFx(SoundName.Touch_Lcd_Button_Ok);
            gameObject.SetActive(false);
        });
        _tgSoundBG.onValueChanged.AddListener(SwitchBG);
        _tgSoundFX.onValueChanged.AddListener(SwitchFX);
    }

    private void SwitchBG(bool val) => Service.SOUND.PlaySoundBG(val);
    private void SwitchFX(bool val) => Service.SOUND.IsSoundFx = val;
}