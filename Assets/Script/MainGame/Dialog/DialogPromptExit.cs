﻿using UnityEngine;
using UnityEngine.UI;

public class DialogPromptExit : DialogBase
{
    [SerializeField] private Button _btnOk, _btnCancel;

    private void Awake()
    {
        _btnOk.onClick.AddListener(() =>
        {
            Service.SOUND.PlaySoundFx(SoundName.Touch_Lcd_Button_Ok);   
            Application.Quit(0);
        });
        _btnCancel.onClick.AddListener(() =>
        {
            Service.SOUND.PlaySoundFx(SoundName.Touch_Lcd_Button_Ok);
            Service.DIALOG.ClickToCancel();
        });
    }
}
