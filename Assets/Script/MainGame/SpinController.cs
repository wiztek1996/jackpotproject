﻿using System;
using System.Collections;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class SpinController : MonoBehaviour
{
    private readonly Image[] _imgBlock = new Image[24];
    [SerializeField] private Button _btnSpin, _btnClear;
    [SerializeField] private Image[] _spinner;
    [SerializeField] private float _speedToSpin;
    [SerializeField] private float _speedStartToSpin;
    [SerializeField] private int _maxSpinCircle = 6;

    private int MaxSpinCircle => Random.Range(_maxSpinCircle - 2, _maxSpinCircle);
    private float SpeedAnimSpin;
    private float _fadeColorDelay = 0.38f;

    private float _speedSecondSpin => _speedToSpin / 1.3f;
    private bool isLuckySpin;
    
    private void Awake()
    {
        InitSpin();
        _btnSpin.onClick.AddListener(ClickToSpin);
        _btnClear.onClick.AddListener(ClickToReset);
        Service.Set(this);
    }


    private void InitSpin()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            _imgBlock[i] = transform.GetChild(i).GetComponent<Image>();
            _imgBlock[i].sprite = LoadFromResources.GetSpriteNormalType(Service.PLAYER.DicBlockdata[i]);
        }
    }

    private void ClickToReset()
    {
        Helper.ClickEffect(_btnClear.transform);
        Service.BET.StatusBtnOptionBet(true);
        SetBtnSpinStatus();
    }

    private void ClickToSpin()
    {
        Helper.ClickEffect(_btnSpin.transform);
        Service.BET.EnableAllBetOption(false);
        _btnSpin.interactable = false;
        _btnClear.interactable = false;
        StartCoroutine(StartSpin(GetSelectedBlock(), _spinner[0]));
        Service.PREVIEW.ActivePreviewImg(false);
    }

    public void StartLuckySpin()
    {
        StopAllCoroutines();
        isLuckySpin = true;
        StartCoroutine(StartSpin(GetSelectedBlock(), _spinner[0]));
        StartCoroutine(StartSecondSpin(GetSelectedBlock(), _spinner[1]));
    }

    private IEnumerator StartSecondSpin(int selectedBlock, Image spinner)
    {
        int step = 0;
        int allStep = 24 * MaxSpinCircle + selectedBlock;
        int slowStep = allStep - 6;
        while (step <= allStep)
        {
            int index = step % 24;
            spinner.transform.position = _imgBlock[index].transform.position;
            yield return new WaitForSeconds(SpeedAnimSpin);
            if (step == allStep)
            {
                spinner.DOFade(0, _fadeColorDelay).SetLoops(3).OnComplete(() =>
                {
                    FinishSpin(selectedBlock);
                    isLuckySpin = false;
                });
                yield break;
            }

            if (step <= 6) // Start 
            {
                SpeedAnimSpin = Mathf.Lerp(SpeedAnimSpin, _speedSecondSpin, 0.3f);
            }
            else if (step >= slowStep) // End 
            {
                SpeedAnimSpin = Mathf.Lerp(SpeedAnimSpin, _speedStartToSpin, 0.3f);
            }
            else
            {
                SpeedAnimSpin = _speedSecondSpin;
            }

            step++;
        }
    }

    private IEnumerator StartSpin(int selectedBlock, Image spinner)
    {
        Debug.LogError("Selected "+selectedBlock);
        int step = 0;
        int allStep = 24 * MaxSpinCircle + selectedBlock;
        int slowStep = allStep - 8;
        while (step <= allStep)
        {
            int index = step % 24;
            spinner.transform.position = _imgBlock[index].transform.position;
            Service.PREVIEW.SetInfo(index);
            yield return new WaitForSeconds(SpeedAnimSpin);
            if (step == allStep)
            {
                spinner.DOFade(0, _fadeColorDelay).SetLoops(3).OnComplete(() => FinishSpin(selectedBlock));
                yield break;
            }

            if (step <= 6) // Start 
            {
                SpeedAnimSpin = Mathf.Lerp(SpeedAnimSpin, _speedToSpin, 0.3f);
            }
            else if (step >= slowStep) // End 
            {
                SpeedAnimSpin = Mathf.Lerp(SpeedAnimSpin, _speedStartToSpin, 0.3f);
            }
            else
            {
                SpeedAnimSpin = _speedToSpin;
            }
            step++;
        }
    }

    private void FinishSpin(int selectedSpin)
    {
        Service.PLAYER.AddCreditById(selectedSpin); // add credit if met Condition
        Service.PREVIEW.SetInfo(Service.PLAYER.GetBlockData(selectedSpin));
        ResetSpin();
        Invoke(nameof(MoveToPlayerCredit), 2.8f);
    }

    private void MoveToPlayerCredit()
    {
        Service.PLAYER.MoveCreditToPlayer();
        Service.BET.StatusBtnOptionBet(false);
        SetSpinnerPos();
    }

  private int GetSelectedBlock()
    {
        int maxNum = 1+1+1+100+2+100+2+100+2+100+14+200+24+200+28+200+10;
        int randomNum = Random.Range(0, maxNum);
        int condINd = 0;
        int bar1 = 1;
        int bar2 = 1;
        int seven1 = 1;
        int seven2 = 100;
        int star1 = 2;
        int star2 = 100;
        int melon1 = 2;
        int melon2 = 100;
        int bell1 = 2/2;
        int bell2 = 100;
        int berry1 = 15/2;
        int berry2 = 200;
        int orange1 = 24/2;
        int orange2 = 200;
        int apple1 = 27/4;
        int apple2 = 200;
        int diamond = 10/2;
        condINd += bar2;
        if (randomNum < condINd) return 0;
        condINd += apple1;
        if (randomNum < condINd) return 1;
        condINd += apple2;
        if (randomNum < condINd) return 2;
        condINd += berry1;
        if (randomNum < condINd) return 3;
        condINd += melon1;
        if (randomNum < condINd) return 4;
        condINd += melon2;
        if (randomNum< condINd) return 5;
        condINd += diamond;
        if (randomNum < condINd) return 6;
        condINd += apple1;
        if (randomNum < condINd) return 7;
        condINd += orange2;
        if (randomNum < condINd) return 8;
        condINd += orange1;
        if (randomNum < condINd) return 9;
        condINd += bell1;
        if (randomNum < condINd) return 10;
        condINd += seven2;
        if (randomNum < condINd) return 11;
        condINd += seven1;
        if (randomNum < condINd) return 12;
        condINd += apple1;
        if (randomNum < condINd) return 13;
        condINd += berry2;
        if (randomNum < condINd)return 14;
        condINd += berry1;
        if (randomNum < condINd) return 15;
        condINd += star2;
        if (randomNum < condINd) return 16;
        condINd += star1;
        if (randomNum < condINd) return 17;
        condINd += diamond;
        if (randomNum < condINd) return 18;
        condINd += apple1;
        if (randomNum < condINd) return 19;
        condINd += bell2;
        if (randomNum < condINd) return 20;
        condINd += orange1;
        if (randomNum < condINd) return 21;
        condINd += bell1;
        if (randomNum < condINd) return 22;
        return 23;
    }

    private void ResetSpin()
    {
        if (!isLuckySpin)
        {
            Service.BET.DicBet.Clear();
            SpeedAnimSpin = _speedStartToSpin;
            _btnClear.interactable = true;
            Service.BET.EnableAllBetOption(true);
        }

        SetSpinnerColor();
    }

    public void SetBtnSpinStatus()
    {
        if (Service.BET.CheckConditionBet()) _btnSpin.interactable = true;
        else _btnSpin.interactable = false;
    }

    public void SetBtnSpinStatus(bool val)
    {
        _btnSpin.interactable = val;
    }

    private void SetSpinnerPos()
    {
        foreach (var spinner in _spinner)
        {
            spinner.transform.localPosition = new Vector3(1000, 1000);
        }
    }

    private void SetSpinnerColor()
    {
        foreach (var spinner in _spinner)
        {
            spinner.color = Color.white;
        }
    }
}