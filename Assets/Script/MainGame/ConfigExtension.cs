﻿using UnityEngine;

public partial class PlayerConfig
{
    public void AddCredit(long creditAdd)
    {
        if (creditAdd > 0)
        {
            WinCredit += creditAdd;
            Service.CREDIT.UpdateWinCredit();
        }
    }

    public void AddPlayerCredit(long creditAdd)
    {
        if (creditAdd > 0)
        {
            _playerCredit += creditAdd;
            Service.CREDIT.UpdatePlayerCredit();
        }
    }

    public long SubCredit()
    {
        long sub = _playerCredit -= DefaultBetValue;
        if (sub >= 0) Service.CREDIT.UpdatePlayerCredit();
        else _playerCredit = 0;
        return sub;
    }

    public void AddCreditById(int i)
    {
        AddCredit(GetCreditById(i));
    }

    private int GetCreditById(int id)
    {
        int credit = 0;
        string result = "LOSE ";
        BetType type = _dicBlockdata[id].type;
        if (type == BetType.LUCKY)
        {
            Service.SOUND.PlaySoundFx(SoundName.Special_Icon);
            Debug.LogWarning("LUCKY BONUS WIN !!!");
            Service.SPIN.StartLuckySpin();
        }
        else
        {
            if (Service.BET.DicBet.ContainsKey(type))
            {
                result = "WIN ";
                BetData betData = Service.BET.DicBet[type];
                Service.BET.FocusWinEffect(type);
                if (_dicBlockdata[id].credit > 0)
                {
                    Service.SOUND.PlaySoundFx(SoundName.Normal_Win);
                    credit += betData.score * _dicBlockdata[id].credit;
                }
                else
                {
                    Service.SOUND.PlaySoundFx(SoundName.Maximum_Win);
                    credit += betData.score * Service.PLAYER.GetBetConfig(type).bonus;
                }
            }
            if(credit == 0 ) Service.SOUND.PlaySoundFx(SoundName.Normal_Lose);
            Debug.LogWarning(" YOU " + result + credit + " Credit IN BLOCK [" + _dicBlockdata[id].type + "]");
        }
        return credit;
    }
}