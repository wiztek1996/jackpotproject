﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PreviewResultBlock : MonoBehaviour
{
   private Image _previewImg;
   private void Awake()
   {
      _previewImg = transform.GetChild(1).GetComponent<Image>();
      ActivePreviewImg(false);
      Service.Set(this);
   }

   public void ActivePreviewImg(bool val = true)
   {
      _previewImg.gameObject.SetActive(val);
   }

   public void SetInfo(BlockData block)
   {
      Helper.ClickLoopEffect(_previewImg.transform,6);
      ActivePreviewImg();
      _previewImg.sprite = LoadFromResources.GetSpritePreviewType(block);
   }
   
   public void SetInfo(int index)
   {
      _previewImg.gameObject.SetActive(true);
      _previewImg.sprite = LoadFromResources.GetSpritePreviewType( Service.PLAYER.GetBlockData(index));
      Service.SOUND.PlaySoundFx(SoundName.Run);
   }
}
