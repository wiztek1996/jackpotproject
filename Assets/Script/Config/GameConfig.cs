﻿using System;
using UnityEngine;

[CreateAssetMenu(fileName = "GameConfig", menuName = "CreateGameConfig/AddGameConfig", order = 1)]
public class GameConfig : ScriptableObject
{
    [Header("PLAYER CREDIT")] public long playerCredit;
    [Header("DEFAULT BET VALUE")] public int defaultBetValue;

    private void OnValidate()
    {
        if (Service.IsSet<PlayerConfig>())
        {
            Service.PLAYER.InitConfig();
            Service.CREDIT.UpdatePlayerCredit();
        }
    }
}