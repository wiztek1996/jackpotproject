﻿using UnityEngine;

public static class LoadFromResources
{
    public static Sprite GetSpriteNormalType(BlockData blockData)
    {
        string multiphiler = "";
        if(blockData.credit > 0)  multiphiler = "x"+blockData.credit;
        return Resources.Load<Sprite>("Normal_type/"+blockData.type+multiphiler);
    }
    
    public static Sprite GetSpritePreviewType(BlockData blockData)
    {
        return Resources.Load<Sprite>("Preview_type/"+blockData.type);
    }
    
    public static Sprite GetSpriteBetType(BetType type)
    {
        return Resources.Load<Sprite>("Bet_type/"+type);
    }
}