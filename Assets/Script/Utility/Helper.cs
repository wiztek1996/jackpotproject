﻿using DG.Tweening;
using System;
using System.Collections;
using System.Diagnostics;
using System.Security.Cryptography;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Object = UnityEngine.Object;

public class Helper
{
    public static string GetIMEI()
    {
        string imei = "";
        imei = SystemInfo.deviceUniqueIdentifier;
        return imei;
    }
    /// <summary>
    /// Call from StartCoroutine()
    /// </summary>
    internal static IEnumerator LoadSceneAsync(string nameScene)
    {
        AsyncOperation asyncOperation = SceneManager.LoadSceneAsync(nameScene);
        asyncOperation.allowSceneActivation = false;
        while (!asyncOperation.isDone)
        {
            // Check if the load has finished
            if (asyncOperation.progress >= 0.9f)
            {
                //Activate the Scene
                asyncOperation.allowSceneActivation = true;
            }
            yield return null;
        }
        yield return null;
    }
    /// <summary>
    /// Call from StartCoroutine()
    /// </summary>
    internal static IEnumerator LoadSceneAsync(string nameScene, Text txtPercent)
    {
        //Begin to load the Scene you specify
        AsyncOperation asyncOperation = SceneManager.LoadSceneAsync(nameScene);
        //Don't let the Scene activate until you allow it to
        asyncOperation.allowSceneActivation = false;
        //When the load is still in progress, output the Text and progress bar
        while (!asyncOperation.isDone)
        {
            //Output the current progress
            txtPercent.text = (asyncOperation.progress * 100) + "%";

            // Check if the load has finished
            if (asyncOperation.progress >= 0.9f)
            {
                //Activate the Scene
                asyncOperation.allowSceneActivation = true;
            }
            yield return null;
        }
        yield return null;
    }


    public static string EncodeSHA1(string pass)
    {
        SHA1CryptoServiceProvider sha1 = new SHA1CryptoServiceProvider();
        byte[] bs = System.Text.Encoding.UTF8.GetBytes(pass);
        bs = sha1.ComputeHash(bs);
        System.Text.StringBuilder s = new System.Text.StringBuilder();
        foreach (byte b in bs)
        {
            s.Append(b.ToString("x1").ToLower());
        }
        pass = s.ToString();
        return pass;
    }

    public static string Base64Decode(string base64EncodedData)
    {
        byte[] base64EncodedBytes = Convert.FromBase64String(base64EncodedData);
        return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
    }
    
    private static string CHUOI_CHECK_VALIDATE = "1234567890_QWERTYUIOASDFGHJKLZXCVBNMqwertyuiopasdfghjklzxcvbnm";//Các kí tự đăng nhập
    /// <summary>
    /// Trả về FALSE nếu không hợp lệ
    /// </summary>
    public static bool CheckUsernameValidate(string username)
    {
        foreach (char kiTu in username)
        {
            bool check = false;
            foreach (char kitu2 in CHUOI_CHECK_VALIDATE)
            {
                if (kiTu == kitu2)
                {
                    check = true;
                    break;
                }
            }
            if (!check)
            { return false; }
        }
        return true;
    }
    private static string CHUOI_CHECK_PASSWORD = "1234567890";
    /// <summary>
    /// Check pasword có cả chữ và số
    /// </summary>
    public static bool CheckPasswordValidate(string password)
    {
        bool checkNumber = false;
        bool checkText = false;
        foreach (char kiTu in password)
        {
            foreach (char kitu2 in CHUOI_CHECK_PASSWORD)
            {
                if (!checkNumber)
                {
                    if (kiTu == kitu2)
                    {
                        checkNumber = true;
                        break;
                    }
                }
            }
        }
        foreach (char kiTu in password)
        {
            if (!char.IsDigit(kiTu))
            {
                checkText = true; break;
            }
        }
        if (checkNumber && checkText)
        {
            return true;
        }
        return false;
    }


    public static string ShowMoney(int money)
    {
        return ConverMoney(long.Parse(money.ToString()));
    }

    public static string ShowMoney(long money)
    {
        return ConverMoney(money);
    }
    public static string ShowMoneyAll(long money)
    {
        return ConverMoneyHu(money);
    }  

    static string ConverMoney(long money)
    {
        string ss = "";
        if (money >= -100L && money <= 100L) return money.ToString();
        else if (money > 100 && money <= 1000000)
        {
            ss = money.ToString("0,0");
        }
        else if (money >= 1000000 && money <= 100000000)
        {
            ss = (money / 1000).ToString("0,0") + "K";
        }
        else if (money >= 100000000)
        {
            ss = (money / 1000000).ToString("0,0") + "M";
        }
        return ss != "00" ? ss : "0";
    }
    static string ConverMoneyHu(long money)
    {
        string ss = "";
        if (money >= -100L && money <= 100L) return money.ToString();
        else if (money > 100 && money <= 1000000)
        {
            ss = money.ToString("0,0");
        }
        else if (money >= 1000000 && money <= 100000000)
        {
            ss = money.ToString("0,0,0")+ "";
        }
        else if (money >= 100000000)
        {
            ss = (money / 1000000).ToString("0,0") + "M";
        }
        return ss != "00" ? ss : "0";
    }
    /// <summary>
    /// Hiện tiền có dấu chấm phân cách
    /// </summary>
    /// <returns></returns>
    public static string ShowMoneyDotDetail(long money)
    {
        return ConverMoneyDotDetail(money);
    }
    private static string ConverMoneyDotDetail(long money)
    {
        string ss = "";
        if (money < 1000L) return money.ToString();
        else if (money >= 1000 && money < 1000000)
        {
            long temp = money / 1000;
            //ss = temp >= 10L ? temp.ToString("0,0") + "K" : temp + "K";
            if (temp >= 0)
            {
                double a = (double)money / 1000L;
                ss = string.Format("{0:0.0}", a);
                string[] arr = ss.Split('.');
                bool ck = false;
                if (int.Parse(arr[1]) == 0)
                {
                    ss = arr[0] + "K";
                    ck = true;
                }
                if (!ck)
                {
                    ss += "K";
                }
            }
            else { ss = temp + "K"; }
        }
        else if (money >= 1000000)
        {
            long temp = money / 1000000;
            if (temp >= 0)
            {
                double a = (double)money / 1000000L;
                ss = string.Format("{0:0.0}", a) + "M";
            }
            else { ss = temp + "M"; }
            //ss = temp >= 10L ? temp.ToString("0,0") + "M" : temp + "M";
        }
        return ss != "00" ? ss : "0";
    }

    public static int RandomNumberGenerator(int min, int max)
    {
        return UnityEngine.Random.Range(min, max);
    }

    /// <summary>
    /// Get color by index màu RGB
    /// </summary>
    public static Color GetColor(float colorIndex)
    {
        Color color = Color.green;
        float max = 255F;
        float r = colorIndex / max;
        float g = colorIndex / max;
        float b = colorIndex / max;
        float a = 1F;
        color = new Color(r, g, b, a);
        return color;
    }
    public static Color GetColor(float _r, float _g, float _b)
    {
        Color color = Color.green;
        float max = 255F;
        float r = _r / max;
        float g = _g / max;
        float b = _b / max;
        float a = 1F;
        color = new Color(r, g, b, a);
        return color;
    }
    /// <summary>
    /// Lấy màu làm mờ ảnh
    /// </summary>
    /// <returns></returns>
    public static Color GetColor()
    {
        Color color = Color.green;
        float m = 255F;
        color = new Color(m, m, m, 0F);
        return color;
    }

    public static void DestroyAllChilds(Transform parent)
    {
        foreach (Transform t in parent)
        {
            Object.Destroy(t.gameObject);
        }
    }

    private static float TIME_EFFECT = 0.6F;
    private static float TIME_PREVIEW_EFFECT = 1.3F;
    private static float TIME_CLICK = 0.3F;
    /// <summary>
    /// Hiệu ứng nhấn nút
    /// </summary>
    public static void ClickEffect(Transform obj,float rate)
    {
        obj.DOScale(new Vector3(rate, rate, rate), TIME_EFFECT).OnComplete(delegate { ReverseScale(obj); });
    }
    public static void ClickEffect(Transform obj)
    {
        obj.DOScale(new Vector3(1.3f, 1.3f, 1.3f), TIME_CLICK).OnComplete(delegate { ReverseScale(obj); });
    }
    
    public static void ClickLoopEffect(Transform obj,int loopCount)
    {
        obj.DOScale(new Vector3(1.18f, 1.18f, 1.18f), TIME_PREVIEW_EFFECT).SetLoops(loopCount).OnComplete(delegate { ReverseScale(obj); });
    }
    private static void ReverseScale(Transform obj)
    {
        obj.DOScale(Vector3.one, TIME_CLICK);
    }
  
    /// <summary>
    /// Gọi trong hàm StartCoroutine()
    /// </summary>
    public static IEnumerator LoadPictureFromInternet(Image img, string linkPicture)
    {
        WWW www = new WWW(linkPicture);
        yield return www;
        img.sprite = Sprite.Create(www.texture, new Rect(0, 0, www.texture.width, www.texture.height), Vector2.zero);
        img.gameObject.SetActive(true);
    }
    /// <summary>
    /// Gọi trong hàm StartCoroutine()
    /// </summary>
    public static IEnumerator DisableObject(GameObject obj, float time)
    {
        yield return new WaitForSeconds(time);
        obj.SetActive(false);
    }

    /// <summary>
    /// Get time miliseconds
    /// </summary>
    /// <returns></returns>
    public static long GetCurrentMilli()
    {
        TimeSpan ts = (DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc));
        long millis = (long)ts.TotalMilliseconds;
        return millis;
    }
    public static string ShowTimeFromMiliseconds(long miliSeconds)
    {
        double ticks = miliSeconds;
        TimeSpan ts = TimeSpan.FromMilliseconds(ticks);
        // Múi h Việt Nam là +7
        DateTime time = new DateTime(1970, 1, 1, 7, 0, 0, DateTimeKind.Utc) + ts; ;
        string format = "HH:mm dd-MM-yyyy";   
        return time.ToString(format);
    }


    public static string NumberGroup(long strNumber)
    {
        return (strNumber <= 0) ? "0" : string.Format("{0:##,##}", strNumber).Replace(",", ".");
    }

    public static void CalculateMethodElapsed(Action func)
    {
        var sw = Stopwatch.StartNew();
        func();
        sw.Stop();
        UnityEngine.Debug.LogError("Time elapsed: " + sw.ElapsedMilliseconds);
    }

    public static void ActiveAllChild(Transform trans,bool val = true)
    {
        for (int i = 0; i < trans.childCount; i++)
        {
            trans.GetChild(i).gameObject.SetActive(val);
        }
    }

}
