﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class TextNumberHelper : MonoBehaviour
{
    public int Time = 1;
    public int Fps = 30;
    private float _timeRate;
    private TextMeshProUGUI _number;

    private long _curNum;
    private long _diffPerSec;
    private long _targetNum;
    private float _diffPerSecDelta;
    private Coroutine _myCoroutine;

    void Awake()
    {
        _number = gameObject.GetComponent<TextMeshProUGUI>();
    }

    void Start()
    {
        _timeRate = 0.5f / Fps;
    }

    public void SetColor(Color color)
    {
        _number.color = color;
    }

    public void IncreaseTo(long x)
    {
        if (_myCoroutine != null)
            StopCoroutine(_myCoroutine);
        if (x > _curNum)
        {
            _diffPerSec = (x - _curNum) / (Fps * Time);
            if (_diffPerSec == 0)
                if ((x - _curNum) > 5) _diffPerSec = (x - _curNum) / 5;
                else _diffPerSec = 1;
            _targetNum = x;
            if (!gameObject.activeInHierarchy)
            {
                SetTo(x);
                return;
            }

            
            _myCoroutine = StartCoroutine(IEIncreaseNumber());
        }

        else
        {
            SetTo(x);
           // StopCoroutine(_myCoroutine);
        }
    }

    public void IncreaseFromZeroTo(long x)
    {
        _curNum = 0;
        _diffPerSec = x / (Fps * Time);
        _targetNum = x;
        if (!gameObject.activeInHierarchy) return;
        if (_myCoroutine != null) StopCoroutine(_myCoroutine);
        _myCoroutine = StartCoroutine(IEIncreaseNumber());
    }

    public void SetTo(long x)
    {
        _curNum = x;
        if (_number != null)
            _number.text = Helper.NumberGroup(_curNum);
    }

    public void SetToNull()
    {
        if (!_number) return;
        _curNum = 0;
        _number.text = "";
    }

    public void SetZero()
    {
        if (!_number) return;
        _curNum = 0;
        _number.text = "0";
    }

    public long GetValue()
    {
        return _curNum;
    }

    IEnumerator IEIncreaseNumber()
    {
        if (!_number)
        {
            yield break;
        }

        while (_curNum < _targetNum)
        {
            _number.text = Helper.NumberGroup(_curNum);
            yield return new WaitForSeconds(_timeRate);
            _curNum += _diffPerSec;
        }

        _curNum = _targetNum;
        _number.text = Helper.NumberGroup(_curNum);
    }

    public void SetColorZdoTextMeshProUGUI()
    {
        //_number.color = new Color(234 / 255f, 52 / 255f, 108 / 255f, 1);
    }

    public void SetColorXuTextMeshProUGUI()
    {
        //_number.color = new Color(1, 1, 1, 1);
    }

    public void SetTextMeshProUGUIColor(Color color)
    {
        _number.color = color;
    }
}