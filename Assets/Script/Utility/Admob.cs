﻿using UnityEngine;
using GoogleMobileAds.Api;

//Banner ad
public class Admob : MonoBehaviour
{
    private BannerView adBanner;

    private const string idApp = "ca-app-pub-4384059551217263~8124858912";
   // private const string idBanner = "ca-app-pub-4384059551217263/3169073378";
    
    private const string idBanner = "ca-app-pub-3940256099942544/6300978111"; //test banner admob


    private void Start()
    {
        // Initialize the Google Mobile Ads SDK.
        MobileAds.Initialize(initStatus => {});
        RequestBannerAd();
    }
    
    #region Banner Methods --------------------------------------------------

    private void RequestBannerAd()
    {
        adBanner = new BannerView(idBanner, AdSize.SmartBanner, AdPosition.Top);
        AdRequest request = AdRequestBuild();
        adBanner.LoadAd(request);
        adBanner.Show();
    }

    #endregion

    private AdRequest AdRequestBuild()
    {
        // Create an empty ad request.
        
         // AdRequest requestBanner = new AdRequest.Builder()
         //     .AddTestDevice(AdRequest.TestDeviceSimulator) // Simulator.
         //     .AddTestDevice("94225930FF3BBAB777217EC3FDDFF936") // My test device.
         //     .Build();
         // return requestBanner;
         return new AdRequest.Builder().Build();
    }
}