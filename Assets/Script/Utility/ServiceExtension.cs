﻿public partial class Service
{
    public static CreditController CREDIT => Get<CreditController>();
    public static PlayerConfig PLAYER => Get<PlayerConfig>();
    public static SpinController SPIN => Get<SpinController>();
    public static BetController BET => Get<BetController>();
    public static PreviewResultBlock PREVIEW => Get<PreviewResultBlock>();
    public static DialogController DIALOG => Get<DialogController>();
    public static SoundManager SOUND => Get<SoundManager>();
}