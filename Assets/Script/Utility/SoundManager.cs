﻿using UnityEngine;


public static class SoundName
{
    public static string Bonus_Money_By_Earning = "Bonus_Money_By_Earning";
    public static string Bonus_Money_By_Time = "Bonus_Money_By_Time";
    public static string Maximum_Win = "Maximum_Win";
    public static string Normal_Lose = "Normal_Lose";
    public static string Normal_Win = "Normal_Win";
    public static string Play = "Play";
    public static string Run = "Run";
    public static string SoundBg = "SoundBg";
    public static string Special_Icon = "Special_Icon";
    public static string Touch_Hard_Button_Disable = "Touch_Hard_Button_Disable";
    public static string Touch_Lcd_Button_Not_Ok = "Touch_Lcd_Button_Not_Ok";
    public static string Touch_Lcd_Button_Ok = "Touch_Lcd_Button_Ok";
    public static string Transfer_Coin = "Transfer_Coin";
}


public class SoundManager : MonoBehaviour
{
    private const string SOUND_PATH = "Sounds/";

    private AudioClip GetSound(string soundName)
    {
        return Resources.Load<AudioClip>(SOUND_PATH + soundName);
    }

    private AudioSource SoundBG;
    private AudioSource SoundFX;
    public bool IsSoundFx { get; set; }

    private void Awake()
    {
        IsSoundFx = true;
        SoundBG = transform.GetChild(0).GetComponent<AudioSource>();
        SoundFX = transform.GetChild(1).GetComponent<AudioSource>();
        PlaySoundBG();
        Service.Set(this);
    }

    public void PlaySoundBG(bool val = true)
    {
        if (val)
        {
            SoundBG.clip = GetSound("SoundBg");
            SoundBG.Play();
        }
        else SoundBG.Stop();
    }
    

    public void PlaySoundFx(string soundName)
    {
        if (!IsSoundFx) return;
        SoundFX.clip = GetSound(soundName);
        SoundFX.Play();
    }

    public void PlaySoundInsertCoin(int i)
    {
        int index = i + 1;
        PlaySoundFx("Insert_Coin_0" + index);
    }
}